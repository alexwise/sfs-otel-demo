---
# general Pandoc options
lang: en-US # change e.g. to 'de-CH' for German (Switzerland)
css: assets/custom.css # put your custom styles in this file

# title slide options
author: Alex Wise
title: OpenTelemetry - A Brief Introduction
subtitle: BLUG November
date: 9 November 2023

# Pandoc options for (reveal.js) slides
revealjs-url: node_modules/reveal.js
theme: black # for possible values see https://revealjs.com/themes/
#background-image: assets/unsplash-lERuUa_96uo.jpg # uncomment for the same background image on every slide

# reveal.js config; for more options like `parallaxBackgroundImage`, see https://revealjs.com/config/
mouseWheel: false
## native presentation size, cf. https://revealjs.com/presentation-size/
width: 1920
height: 1080

# additional reveal.js plug-in config
## progress bar of elapsed time
## see https://github.com/tkrkt/reveal.js-elapsed-time-bar#configurations
elapsedtimebar:
    enable: true
    allottedTime: 15 * 60 * 1000 # equals 15 min; unit is milliseconds
    progressBarHeight: 3 # unit is pixels
    barColor: 'rgb(200,0,0)'
    pausedBarColor: 'rgba(200,0,0,.6)'
## highlight the current mouse position with a spotlight
## see https://github.com/denniskniep/reveal.js-plugin-spotlight#configuration
spotlight:
    enable: true
    size: 60 # size of the spotlight
    lockPointerInsideCanvas: false # lock the mouse pointer inside the presentation
    togglePresentationModeKeyCode: 77 # [keyCode](https://developer.mozilla.org/docs/Web/API/KeyboardEvent/keyCode) to toggle presentation mode; visit <https://keycode.info/> to easily determine keyCodes (77 = m); disabled when set to `false`
    toggleSpotlightOnMouseDown: true # toggle spotlight by holding down the mouse key
    spotlightOnKeyPressAndHold: false # [keyCode](https://developer.mozilla.org/docs/Web/API/KeyboardEvent/keyCode) to toggle spotlight; visit <https://keycode.info/> to easily determine keyCodes; disabled when set to `false`
    spotlightCursor: 'none' # the cursor when spotlight is on; e.g. 'crosshair'
    presentingCursor: 'none' # the cursor when spotlight is off and in presentation mode; e.g. 'default'
    initialPresentationMode: true # start the presentation in presentation mode
    disablingUserSelect: true # disable text selection in presentation mode
    fadeInAndOut: 100 # transition duration; unit is milliseconds; disabled when set to `false`
    useAsPointer: false # use a pointer instead of a spotlight
    pointerColor: 'red' # only relevant if `useAsPointer: true`
## indicators to show the amount of slides in a vertical stack
## see https://github.com/Martinomagnifico/reveal.js-verticator#configuration
verticator:
    enable: true
    darktheme: true # set to `false` if you use a light theme
    color: '' # manually set the normal verticator color
    oppositecolor: '' # manually set the inverted verticator color
    skipuncounted: true # Omit drawing Verticator bullets for slides that have `data-visibility="uncounted"` set?
    clickable: true # Allow navigation to a slide by clicking on the corresponding Verticator bullet?
---

# My primary hopes for this talk

- Focus on why OpenTelemetry exists
- Give a high-level overview of what it can do
- Show folks some tools they can use to explore more
- Discuss challenges and tradeoffs using OpenTelemetry in production


# $ whoami

- SWE focused on the reliability of distributed systems
- Software Freedom School
- Learning From Incidents
- Verica/Prowler/VOID Advisory Network
- An OpenTelemetry user

## whoaminot

- An OpenTelemetry contributor
- A monitoring company salesperson
- A Deep expert of OTel internals

# The Ancient Times
(You know, like 8 years ago)

![Look on my works, ye mighty, and despair!](images/ozymandias.png)

## There were a few ways to understand what your application was up to while it was running...

## Logs {data-transition="convex" data-visibility="uncounted"}

- A generally-accepted standard since the 80s

- Highly cardinal events

## Metrics {data-transition="convex"}

- Mostly host-level

- Aggregateable, bucketed, time-series

## Remote Debugging {data-transition="convex"}

At a certain scale and compliance level works great

# This created fertile ground for monitoring companies

Logs and Host-level Metrics are relatively easy to standardize on.

Therefore, a lot of providers popped up to help wrangle this data. 

##

![What's inside the black box?](images/blackbox.png)

# Beyond the Black Box

> "What if we had the application tell us its view of the world? That would make it so easy to identify and debug stuff!"

##

Can no longer have a single agent that works for everyone! You need libraries _inside_ the app!

## The Start of Observability

All the major monitoring companies started releasing "Application Performance Monitoring" libraries for popular languages.

(But there are always more languages)

## Having the application emit meaningful data has its perks

![Waterfall diagram of a trace in jaeger](images/trace.png)

## The Problem with APM (for everyone involved!)

![From "A Brief History of OpenTelemetry (So Far), CNCF](images/foreach.png)

##

![Example of how to use the Elastic APM Gormv2 Libary](images/elasticgorm.png)

This is one of a dozen or so libraries to instrument a Go application for Elastic APM. (This is worst-case, some languages are much easier)

## Is This DevOps?

# OpenCensus and OpenTracing

Complexity was increasing, and the amount of work to do grew exponentially. Even BigCos couldn't manage the sprawl, not to mention monitoring vendors and even popular open source projects like Prometheus.

OpenTracing (2015) and OpenCensus (2017) were founded to attempt to create an extensible standard.

## The Two Projects merged in 2019 to become OpenTelemetry

> OpenTracing and OpenCensus have led the way in that effort, and while each project made different architectural choices, **the biggest problem with either project has been the fact that there were two of them.**

_A Brief History of OpenTelemetry (So Far), https://www.cncf.io/blog/2019/05/21/a-brief-history-of-opentelemetry-so-far/_

# What exactly is it?

At its core, the OpenTelemetry project is a set of API, protocol, and data specifications that allow a variety of tools and providers to send and receive observability data.

They also contribute engineering effort to a library ecosystem to implement OTel in various languages and applications. And participate on the steering committee for some languages to drive compatibility and performance.

## The Data

OpenTelemetry Data is one of four "Signals," and it's transmission is specified by the OpenTelemetry Line Protocol (OTLP).

The four Signals are:

- Logs
- Metrics
- Traces
- Baggage

## Baggage

![Screenshot of Baggage Structure](images/otel-baggage.svg)

# The Collector

![Movie Poster for The Collector](images/thecollector.jpg)


## Sorry, This Collector

![Diagram of OTel Collector](images/otel-collector.svg)


## OTel Collector

The OpenTelemetry Collector is a containerized deployment that is able to receive data from a variety of sources, perform processing and pipelining, and transmitting it in OTLP. 

There are three types of components:

- [Receivers](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/receiver) (scraping Prometheus endpoints or receiving logs from a parser)
- [Processors](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/processor) (batching data together or limiting memory use)
- [Exporters](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/exporter) (Sending data to providers (Elastic, DataDog, Jaeger))

# The Architecture

![App to OTelCol](images/app-to-otelcol.png)

##

<section>
  <pre><code data-trim data-noescape>
config:
  exporters:
    prometheus:
      endpoint: "0.0.0.0:8889"
      const_labels:
        app: $app
    logging: {}
  extensions:
    health_check: {}
  processors:
    batch: {}
    # If set to null, will be overridden with values based on k8s resource limits
    memory_limiter:
      limit_percentage: 50
      check_interval: 1s
      spike_limit_percentage: 30
  receivers:
    prometheus:
      config:
        scrape_configs:
          - job_name: container1
            scrape_interval: 10s
            static_configs:
              - targets:
                  - ${MY_POD_IP}:9090
          - job_name: container2
            scrape_interval: 10s
            static_configs:
              - targets:
                  - ${MY_POD_IP}:9091
      service:
        pipelines:
          metrics:
            receivers: [prometheus]
            processors: [batch, memory_limiter]
            exporters: [prometheus]
        extensions:
          - health_check
  </code></pre>
</section>

# Getting Started

## Playing Around on the Command Line 

Amy Tobey has written otel-cli for easy creation of spans and shipping ad-hoc stuff to a collector.

https://github.com/equinix-labs/otel-cli

## Let's Get Our Hands Dirty

The OpenTelemetry project maintains a comprehensive ["Astronomy Shop"](https://opentelemetry.io/docs/demo/architecture/) microservices demo that shows many features of OpenTelemetry in 12 different languages

It covers metrics and traces (not logs yet) and provides traffic generation. Can be deployed locally with Docker Compose or Kubernetes

# Challenges in Production
A lot of the same as other monitoring solutions

* Tradeoffs between Cardinality, Dimensionality, and Cost
* Scaling the Collector
* Maintaining Backpressure
* Autoinstrument if your language supports it
* Try to get fully e2e coverage of your requests
* Sample your traces!


# Thanks! {data-background-image=assets/unsplash-_g1WdcKcV3w.jpg}

# Extra Stuff

## There Are No Three Pillars

> I’ve often pointed out that observability is built on top of arbitrarily wide structured data blobs, and that metrics, logs, and traces can be derived from those blobs while the reverse is not true—you can’t take a bunch of metrics and reformulate a rich event.

 - Charity Majors, "THE TRUTH ABOUT “MEH-TRICS”"

## 

```md
| Logs    | Ad-hoc context of discrete events            |
|---------|----------------------------------------------|
| Metrics | Aggregations and low-cardinality time series |
|---------|----------------------------------------------|
| Traces  | Relationships between events, timings        |
```

## Slides

`1n.pm/...`
