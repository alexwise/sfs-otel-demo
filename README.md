To view slides, open index.html in a broswer.

Slides created using [pandoc](https://pandoc.org)

To regenerate index.html, install pandoc and run the following command:

```
$ pandoc -t revealjs -s -o index.html slides.md -V revealjs-url=https://unpkg.com/reveal.js/ --include-in-header=slides.css -V theme=serif
```
